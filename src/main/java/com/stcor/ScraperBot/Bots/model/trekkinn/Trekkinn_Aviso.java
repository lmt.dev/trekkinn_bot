/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stcor.ScraperBot.Bots.model.trekkinn;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Lucas
 */

@Entity
public class Trekkinn_Aviso implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;
    private Date alta;
    private String nombre;
    private String link;
    private String foto;
    private String precioTach;
    private String precioNeto;
    private String talla;
    private String precioEnvio;
    public Date ultimaActualizacion;
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the alta
     */
    public Date getAlta() {
        return alta;
    }

    /**
     * @param alta the alta to set
     */
    public void setAlta(Date alta) {
        this.alta = alta;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the foto
     */
    public String getFoto() {
        return foto;
    }

    /**
     * @param foto the foto to set
     */
    public void setFoto(String foto) {
        this.foto = foto;
    }

    /**
     * @return the precioTach
     */
    public String getPrecioTach() {
        return precioTach;
    }

    /**
     * @param precioTach the precioTach to set
     */
    public void setPrecioTach(String precioTach) {
        this.precioTach = precioTach;
    }

    /**
     * @return the precioNeto
     */
    public String getPrecioNeto() {
        return precioNeto;
    }

    /**
     * @param precioNeto the precioNeto to set
     */
    public void setPrecioNeto(String precioNeto) {
        this.precioNeto = precioNeto;
    }

    /**
     * @return the talla
     */
    public String getTalla() {
        return talla;
    }

    /**
     * @param talla the talla to set
     */
    public void setTalla(String talla) {
        this.talla = talla;
    }
    
        /**
     * @return the precioEnvio
     */
    public String getPrecioEnvio() {
        return precioEnvio;
    }

    /**
     * @param precioEnvio the precioEnvio to set
     */
    public void setPrecioEnvio(String precioEnvio) {
        this.precioEnvio = precioEnvio;
    }
    
    /**
     * @return the ultimaActualizacion
     */
    public Date getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    /**
     * @param ultimaActualizacion the ultimaActualizacion to set
     */
    public void setUltimaActualizacion(Date ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }
}
