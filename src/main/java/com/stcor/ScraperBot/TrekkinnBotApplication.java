package com.stcor.ScraperBot;

import com.stcor.ScraperBot.Services.BotService;
import com.stcor.ScraperBot.Services.Trekkinn_Service;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@SpringBootApplication
public class TrekkinnBotApplication implements CommandLineRunner {
	String selectedService = "";

	public static void main(String[] args) {
		SpringApplication.run(TrekkinnBotApplication.class, args);

	}
	@Bean
	public BotService getBotService(){
		switch (selectedService){
			default:
				return new Trekkinn_Service();
		}
	}

	@Override
	public void run(String... args) throws Exception {
		getBotService().start();
	}

}
